import os
from pathlib import Path

__template__ = """
{board}:
  <<: *nrf
"""

yaml = Path(__file__).parent / ".gitlab-ci.yml"
ci_script = yaml.read_text()

boards_dir = Path(__file__).parent / ".." / ".." / "micropython" / "ports" / "nrf" / "boards"
boards = [p.name for p in boards_dir.iterdir() if p.is_dir()]

ci_script_divider = '#############'
ci_script_parts = ci_script.split(ci_script_divider)
ci_script = ci_script_divider.join(ci_script_parts[:-1]) + ci_script_divider

updated = []

for board in sorted(boards):
    definition = __template__.format(board=board)

    if definition not in ci_script_parts[-1]:
        updated.append(board)
    
    ci_script += definition

if updated:
    yaml.write_text(ci_script)
    os.system("git add %s" % yaml)
    os.system('git commit -m "Added nrf boards: %s"' % ",".join(updated))
